import {Client} from 'pg';
import { DB } from './db';
import { CardDto } from "../common/dtos";

interface Options {
    username:string;
    password:string;
    database:string;
    host?:string;
    port?:number;
}

export class PgSQL implements DB {

    private client: Client;

    constructor(opts:Options) {
        this.client = new Client({
            user: opts.username,
            password: opts.password,
            database: opts.database,
            host: opts.host || 'localhost',
            port: opts.port || 5432
        });

        this.client.connect();
    }

    private asyncQuery(query:string, values:any[] = []):Promise<any[]> {
        return new Promise((resolve, reject) => {
            this.client.query(query, values, (err, result) => {
                if (err !== null) return reject(err);
                resolve(result.rows);
            });
        });
    }

    private async asyncQueriesInTransaction(queries:[string,any[]][]) {
        try {
            await this.asyncQuery('BEGIN');
            queries.forEach(async query => {
                if (typeof query === 'string') {
                    await this.asyncQuery(<string>query);
                } else {
                    await this.asyncQuery(query[0], query[1]);
                }
            });
            await this.asyncQuery('COMMIT');
        } catch (e) {
            console.error(e);
        }
    }

    getAllTickets() {
        return new Promise<CardDto[]>(async (resolve, reject) => {
            try {
                let results = await this.asyncQuery(
                    'SELECT key, coords, parent '+
                    'FROM issuemetadata i '+
                    'LEFT JOIN hierarchy h ON h.child = i.key'
                );
                resolve(results.map(result => new CardDto({
                    x: result.coords.x,
                    y: result.coords.y,
                    key: result.key,
                    parent: result.parent
                })));
            } catch (e) {
                reject(e);
            }
        });
    }

    updateCards(cardDtos) {
        const UPDATE_CARD_QUERY = 'UPDATE issuemetadata SET coords = $2 WHERE key = $1';

        let queries:[string,string[]][] = [];
        cardDtos.forEach(cardDto=>{
            queries.push([
                UPDATE_CARD_QUERY,
                [cardDto.key, `(${cardDto.x}, ${cardDto.y})`]
            ]);
        });
        //noinspection JSIgnoredPromiseFromCall
        this.asyncQueriesInTransaction(queries);
    }
}
