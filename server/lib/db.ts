import {CardDto} from '../common/dtos';

export interface DB {
    getAllTickets():Promise<CardDto[]>
    updateCards(cardDtos:CardDto[]):void
}
