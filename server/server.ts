import * as express from 'express';
import {Response, Request} from 'express';
import * as bodyParser from 'body-parser';
import {PgSQL} from './lib/pgsql';
import {DB} from './lib/db';
const CONFIG  = require('./config.json');

const app = express();
const db:DB = new PgSQL(CONFIG.pgsql);

const PORT = 3000;

let longPolls:Response[] = [];

app.use(bodyParser.json());
app.use('/', express.static('../client/dist'));

app.patch('/rest/cards', (req,res)=>{
    res.end();

    db.updateCards(req.body);

    // TODO skip the requester's longpoll
    let longPoll;
    while(longPoll = longPolls.shift()) {
        longPoll.send({continueLongPoll:true, data:req.body});
    }
});

app.get('/rest/cards', async (req,res)=>{
    try {
        res.send(await db.getAllTickets());
    } catch (e) {
        console.error(e);
    }
});

app.get('/poll', (req,res)=>{
    longPolls.push(res);
});

app.listen(PORT, _=>{
    console.log(`started on port ${PORT}`);
});

// TODO check Jira for updates between restarts
// TODO webhook for Jira to push updates to
