# Board

Board is an attempt of making a virtual kanban board, just like you would with post-it notes in a physical wall.

## Development

You need async/await support on both your server and browser. So that means [NodeJS 7.6](https://nodejs.org/en/blog/release/v7.6.0/) and [probably a pretty shiny version of Chrome](http://caniuse.com/#feat=async-functions) (anything using [V8 v5.5](https://v8project.blogspot.fi/2016/10/v8-release-55.html) or newer is golden).

You also need PostgreSQL, and make `server/config.json` (by e.g. copying `server/config.example.json`) and add relevant database info in there.

Anyways, to get things going, clone the project and run either

    $ yarn install
    $ gulp
    $ yarn run start

or 

    $ npm install
    $ gulp
    $ npm run start
