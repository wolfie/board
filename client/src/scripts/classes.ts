import {Dto, CardDto, DtoWrapper, dtoFactory} from '../common/dtos';

export {Dto, CardDto, DtoWrapper}

class Rectangle {
    static overlap(a:Rectangle, b:Rectangle):boolean;
    static overlap(a:ClientRect, b:ClientRect):boolean;
    static overlap(a:Rectangle, b:ClientRect):boolean;
    static overlap(a:ClientRect, b:Rectangle):boolean;
    static overlap(a, b):boolean {
        return !(a.right < b.left || b.right < a.left || a.bottom < b.top || b.bottom < a.top);
    }

    x1 = -1;
    y1 = -1;
    x2 = -1;
    y2 = -1;

    get x() { return this.x1 }
    get y() { return this.y1 }

    get left() { return Math.min(this.x1,this.x2) }
    get top() { return Math.min(this.y1, this.y2) }

    get right() { return Math.max(this.x1, this.x2) }
    get bottom() { return Math.max(this.y1, this.y2) }

    get height() { return this.bottom - this.top }
    get width() { return this.right - this.left }

    clear():void {
        this.x1 = this.y1 = this.x2 = this.y2 = -1;
    }

    overlaps(another:Rectangle):boolean;
    overlaps(another:ClientRect):boolean;
    overlaps(another):boolean {
        return Rectangle.overlap(this, another);
    }
}

export class SelectionBox {
    private s:CSSStyleDeclaration;
    private rect = new Rectangle();
    constructor (private e:HTMLElement) {
        this.s = e.style;
    }

    setDragOrigin(x:number, y:number):void {
        this.rect.x1 = x;
        this.rect.y1 = y;
        this.setDragCorner(x,y);
    }

    setDragCorner(x:number, y:number):void {
        this.rect.x2 = x;
        this.rect.y2 = y;

        this.s.top = this.rect.top+'px';
        this.s.left = this.rect.left+'px';
        this.s.height = this.rect.height+'px';
        this.s.width = this.rect.width+'px';
    }

    clear():void {
        this.rect.clear();
        this.s.width = this.s.height = this.s.left = this.s.top = '';
    }

    overlaps(another:ClientRect) {
        return this.rect.overlaps(another);
    }
}

export class DomCard extends DtoWrapper<CardDto> {
    static INSTANCES = new Map<string,DomCard>();
    static readonly CLASS_NAME = 'card';

    static recognizesElement(element) {
        return element.classList.contains(DomCard.CLASS_NAME);
    }

    static fromElement(element:Element): DomCard {

        for (const card of DomCard.INSTANCES.values()) {
            if (card.e === element) return card;
        }
        return null;
    }

    /**
     * @param {string} key 
     * @returns {DomCard|null}
     */
    static fromKey(key) {
        const element = document.querySelector(`.${DomCard.CLASS_NAME}[data-key="${key}"]`);
        if (element) {
            return DomCard.fromElement(element);
        } else {
            return null;
        }
    }

    static linkRelationships() {
        DomCard.INSTANCES.forEach(childCard=>{
            const parentKey = childCard.dto.parent;
            if (parentKey !== null) {
                const parentCard = DomCard.INSTANCES.get(parentKey);
                parentCard.children.add(childCard);
                childCard.parent = parentCard;
            }
        });
    }

    e:HTMLDivElement = null;
    popup:Popup = null;
    children = new Set<DomCard>();
    parent: DomCard = null;

    attach() {
        if (this.e === null) {
            this.e = document.createElement('div');
            this.e.classList.add(DomCard.CLASS_NAME);
            this.e.dataset.key = this.dto.key;
            this.moveTo(this.dto.x, this.dto.y);
        }
        
        if (this.e.parentElement) {
            console.warn(`Can't attach: ${this.constructor.name} was already attached`);
            return;
        }

        document.body.appendChild(this.e);
        DomCard.INSTANCES.set(this.dto.key, this);
        if (this.popup) {
            this.popup.attach();
        }
    }

    detach() {
        if (this.popup) {
            this.popup.detach();
        }
        document.body.removeChild(this.e);
        DomCard.INSTANCES.delete(this.dto.key);
    }

    moveTo(x:number, y:number) {
        this.dto.x = x;
        this.dto.y = y;
        this.e.style.left = `${x}px`;
        this.e.style.top = `${y}px`;
        if (this.popup) {
            this.popup.moveTo(x, y);
        }
    }

    togglePopup() {
        if (this.popup === null) {
            this.popup = new Popup(this);
            this.popup.moveTo(this.dto.x, this.dto.y);
            if (this.e.parentElement) this.popup.attach();
        } else {
            this.popup.detach();
            this.popup = null;
        }
    }
}

class Popup {
    static readonly CLASS_NAME = 'popup';
    static readonly HEIGHT = 90;

    readonly e:HTMLDivElement;
    constructor(private card:DomCard) {
        /** @type {HTMLDivElement} */
        this.e = document.createElement('div');
        this.e.textContent = `Loading content for ${this.card.dto.key}...`;
        this.e.classList.add(Popup.CLASS_NAME);
    }

    attach() {
        if (this.e.parentElement) {
            console.warn(`Can't attach: ${this.constructor.name} was already attached`);
            return;
        }

        this.updateContent();
        document.body.appendChild(this.e);
    }

    detach() {
        document.body.removeChild(this.e);
    }

    moveTo(x:number, y:number) {
        this.e.style.left = `${x}px`;
        this.e.style.top = `${y - Popup.HEIGHT}px`;
    }

    private updateContent() {
        this.e.textContent = `Popup for ${this.card.dto.key}`;
    }
}

export function deflate<T extends Dto>(entities:DtoWrapper<T>[]):T[];
export function deflate<T extends Dto>(entity:DtoWrapper<T>):T;
export function deflate(entity) {
    if (Array.isArray(entity)) {
        return entity.map(hasDto => hasDto.dto);
    } else {
        return entity.dto;
    }
}

function inflateSingle(dtoLike:any):Dto {
    return dtoFactory(dtoLike.$type, dtoLike);
}

export function inflate(dtoLikes:any[]):Dto[];
export function inflate(dtoLike:any):Dto;
export function inflate(object) {
    if (Array.isArray(object)) {
        return object.map(inflateSingle);
    } else {
        return inflateSingle(object);
    }
}
