import {ClientToServerConnection} from './clientToServerConnection';
import {SelectionBox, DomCard} from './classes';

export {SelectionBox, ClientToServerConnection}
export {inflate, deflate, CardDto, DomCard} from './classes';

declare const SELECTIONBOX: SelectionBox;
declare const CLIENT_TO_SERVER_CONNECTION: ClientToServerConnection;

const CLICK_THRESHOLD = 2;

let moveEventsCounter = 0;

let cardUnderPointerDown:DomCard = null;

interface Handler {
    onPointerDown(e:PointerEvent);
    onPointerMove(e:PointerEvent);
    onPointerUp(e:PointerEvent);
}

class SelectionHandler implements Handler {
    onPointerDown(e) {
        SELECTIONBOX.setDragOrigin(e.pageX, e.pageY);
    }

    onPointerMove(e) {
        SELECTIONBOX.setDragCorner(e.pageX,e.pageY);
        let elem = document.body.firstElementChild;
        do {
            if (!DomCard.recognizesElement(elem)) continue;
            // TODO: factor a selection model out of this
            elem.classList.toggle('selected', SELECTIONBOX.overlaps(elem.getBoundingClientRect()));
        } while (elem = elem.nextElementSibling);
    }

    onPointerUp(e) {
        SELECTIONBOX.clear();
    }
}

class CardMoveHandler implements Handler {
    private movingCardOffsets = new Map<DomCard, {x:number, y:number}>();

    onPointerDown(e) {
        let selectedCards = findSelectedCards();

        if (selectedCards.length === 0) {
            selectedCards.push(cardUnderPointerDown);
        } else if (selectedCards.indexOf(cardUnderPointerDown) < 0) {
            deselectAll();
            selectedCards = [cardUnderPointerDown];
        }

        selectedCards.forEach(card=>{
            (<any>card.e.style).willChange = 'top,left';
            this.movingCardOffsets.set(card, {
                x: e.pageX - card.dto.x,
                y: e.pageY - card.dto.y
            });
        });
    }

    onPointerMove(e) {
        this.movingCardOffsets.forEach((offset, card)=>{
            const newX = e.pageX-offset.x;
            const newY = e.pageY-offset.y;
            card.moveTo(newX, newY);
        });
    }

    onPointerUp(e) {
        CLIENT_TO_SERVER_CONNECTION.updateCards([...this.movingCardOffsets.keys()]);
        this.movingCardOffsets.forEach((_,card)=>{
            (<any>card.e.style).willChange = '';
        });
        this.movingCardOffsets.clear();
    }
}

class HoverHandler implements Handler {
    static readonly INSTANCE = new HoverHandler();
    static readonly PARENT_HIGHLIGHT_CLASS = 'parentHighlight';
    static readonly CHILD_HIGHLIGHT_CLASS = 'childHighlight';
 
    onPointerDown(e) {}

    onPointerMove(e) {
        DomCard.INSTANCES.forEach(card=>{
            card.e.classList.remove(HoverHandler.PARENT_HIGHLIGHT_CLASS);
            card.e.classList.remove(HoverHandler.CHILD_HIGHLIGHT_CLASS);
        });
        if (DomCard.recognizesElement(e.target)) {
            const target = DomCard.fromElement(e.target);
            let parent = target.parent;
            if (parent) {
                parent.e.classList.add(HoverHandler.PARENT_HIGHLIGHT_CLASS);
                parent.children.forEach(sibling=>sibling.e.classList.add(HoverHandler.CHILD_HIGHLIGHT_CLASS));
            } else if (target.children.size > 0) {
                target.e.classList.add(HoverHandler.PARENT_HIGHLIGHT_CLASS);
                target.children.forEach(child=>child.e.classList.add(HoverHandler.CHILD_HIGHLIGHT_CLASS));
            }
        }
    }

    onPointerUp(e) {}
}

let handler:Handler = HoverHandler.INSTANCE;

window.addEventListener('pointerdown', e=>{
    const targetElement = <Element>e.target;
    // only hijack events for targets that we have explicit actions for
    if (!(
        targetElement.tagName === 'HTML'
        || DomCard.recognizesElement(e.target)
        )) return;

    e.preventDefault();
    moveEventsCounter = 0;

    if (DomCard.recognizesElement(e.target)) {
        cardUnderPointerDown = DomCard.fromElement(targetElement);
        handler = new CardMoveHandler();
    } else {
        handler = new SelectionHandler();
    }
    handler.onPointerDown(e);
});

window.addEventListener('pointermove', e=>{
    if (handler === null) return;
    e.preventDefault();

    moveEventsCounter++;
    handler.onPointerMove(e);
});

window.addEventListener('pointerup', e=>{
    if (handler === null) return;
    e.preventDefault();

    const treatAsClick = (moveEventsCounter <= CLICK_THRESHOLD);
    if (treatAsClick) {
        if (!cardUnderPointerDown && handler instanceof SelectionHandler) {
            // this gets rid of a leftover selection box.
            handler.onPointerUp(e);
        }
        deselectAll();
        if (cardUnderPointerDown) {
            cardUnderPointerDown.togglePopup();
        }
    } else {
        handler.onPointerUp(e);
    }

    handler = HoverHandler.INSTANCE;

    cardUnderPointerDown = null;
});

window.addEventListener('keydown', e=>{
    if (e.key === 'Shift') {
        document.body.classList.add('resizingCards');
    }
});

window.addEventListener('keyup', e=>{
    if (e.key === 'Shift') {
        document.body.classList.remove('resizingCards');
    }
});

function findSelectedCards():DomCard[] {
    const selection = <DomCard[]>[];
    let elem = document.body.firstElementChild;
    do {
        // TODO: factor a selection model out of this
        if (DomCard.recognizesElement(elem) && elem.classList.contains('selected')) {
            selection.push(DomCard.fromElement(elem));
        }
    } while (elem = elem.nextElementSibling);
    return selection;
}

function deselectAll() {
    const results = document.querySelectorAll('.card.selected');
    for (let i=0; i<results.length; i++) {
        results[i].classList.remove('selected');
    }
}
