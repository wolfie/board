import {CardDto, DtoWrapper, DomCard, Dto} from './classes'

interface RequestOpts {
    url: string;
    method?: string;
    headers?: {[headerName:string]: string};
    params?: any;
}

export class ClientToServerConnection {
    /* from http://stackoverflow.com/a/30008115/2238 */
    private static makeRequest(opts:RequestOpts):Promise<any> {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open(opts.method || 'GET', opts.url);
            xhr.onload = function () {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({
                        status: xhr.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            };
            if (opts.headers) {
                for (let headerName in opts.headers) {
                    xhr.setRequestHeader(headerName, opts.headers[headerName]);
                }
            }
            var params = opts.params;
            // We'll need to stringify if we've been given an object
            // If we have a string, this is skipped.
            if (params && typeof params === 'object') {
                xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
                params = JSON.stringify(params);
            }
            xhr.send(params);
        });
    }


    private static longPoll(url:string, callback: (data:any) => any) {
        let stopPolling = false;
        let consecutiveErrors = 0;

        const pollLoop = async function() {
            if (stopPolling) {
                console.log('Long polling aborted as per request');
                return;
            }

            try {
                const result = JSON.parse(await ClientToServerConnection.makeRequest({
                    url: url,
                    method: 'GET'
                }));

                callback(result.data);

                if (stopPolling) {
                    console.log('Will not renew long poll');
                } else if (!result.continueLongPoll) {
                    console.log('Server did not want another long poll');
                } else {
                    setTimeout(pollLoop, 0);
                }
            } catch (e) {
                console.error('Long poll failed, because of the following error:');
                console.error(e);

                if (consecutiveErrors === 0) setTimeout(_=>{
                    consecutiveErrors = 0;
                }, 10000);

                consecutiveErrors++;
                if (consecutiveErrors < 5) {
                    console.error('Reopening long poll');
                    setTimeout(pollLoop, 0);
                } else {
                    console.error('Too many failures too frequently. No more retries');
                }
            }
        };

        setTimeout(pollLoop, 0);

        return function() {
            console.log('long poll abort requested');
            stopPolling = true;
        };
    }

    private abortLongPoll:Function = null;
    constructor(private inflateFunction: <T extends Dto>(objects:T[]) => DtoWrapper<T>[], private deflateFunction: <T extends Dto>(dtos:DtoWrapper<T>[]) => T[]) {}


    startLongPolling() {
        if (this.abortLongPoll !== null) {
            console.warn('Cannot start long polling: it is already started');
            return;
        }

        this.abortLongPoll = ClientToServerConnection.longPoll('/poll', (response) => {
            const updatedDeflatedCards = response;

            updatedDeflatedCards.forEach(deflatedCard=>{
                const inflatedCard = DomCard.fromKey(deflatedCard.key);
                if (inflatedCard) {
                    inflatedCard.moveTo(deflatedCard.x, deflatedCard.y);
                }
            });
        });
    }

    stopLongPolling() {
        if (this.abortLongPoll === null) {
            console.warn('Cannot stop long polling: it is not started');
            return;
        }

        this.abortLongPoll();
        this.abortLongPoll = null;
    }

    fetchAllCardDtos():Promise<CardDto[]> {
        return new Promise(async (resolve, reject)=>{
            try {
                const response = await ClientToServerConnection.makeRequest({
                    url: '/rest/cards'
                });

                const cards = <CardDto[]>JSON.parse(response).map(this.inflateFunction);
                resolve(cards);
            } catch (e) {
                reject(e);
            }
        })
    }

    updateCards(cards:DomCard[]):void {
        ClientToServerConnection.makeRequest({
            method: 'PATCH',
            url: '/rest/cards',
            params: this.deflateFunction(cards)
        });
    }
}
