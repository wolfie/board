// [common]

interface Config {
    x:number;
    y:number;
    key:string;
    parent?:string;
}

export class DtoWrapper<T extends Dto> {
    constructor(readonly dto:T) {}
}

export interface Dto {
    readonly $type:string;
}

export class CardDto implements Dto {
    readonly $type = this.constructor.name;
    x:number;
    y:number;
    key:string;
    parent?:string;

    constructor(x:number, y:number, key:string, parent?:string);
    constructor(opts:Config);
    constructor(x:number|Config, y:number=null, key:string=null, parent:string=null) {
        if (typeof x === 'object') {
            this.x = x.x;
            this.y = x.y;
            this.key = x.key;
            this.parent = x.parent||null;
        } else {
            this.x = x;
            this.y = y;
            this.key = key;
            this.parent = parent;
        }
    }
}

export function dtoFactory(className:string, dtoLike:any):Dto {
    switch (className) {
        case CardDto.name: return new CardDto(dtoLike);
        default: throw new Error(`Unrecognized DTO: ${className}`);
    }
}