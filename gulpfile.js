const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const replace = require('gulp-replace');
const runSequence = require('run-sequence');

gulp.task('clean:client', ()=>{
    return del([
        "client/src/common",
        "client/dist/**/*"
        ]);
});

gulp.task('clean:server', ()=>{
    return del([
        "server/common",
        "server/*.js",
        "server/lib/*.js",
    ]);
});

const client = ts.createProject('tsconfig.client.json');
gulp.task('compile:client', ()=>{
    const result = client.src().pipe(client())
    return result.js.pipe(gulp.dest('client/dist'));
});

gulp.task('copy:client', ()=>gulp
    .src(['./client/src/**/*', '!./client/src/**/*.ts'], {base: './client/src'})
    .pipe(gulp.dest('./client/dist'))
);

gulp.task('build:client', (done)=>{
    runSequence(
        'clean:client', 
        'copy:common',
        ['compile:client', 'copy:client'], 
        done);
});

const server = ts.createProject('tsconfig.server.json');
gulp.task('compile:server', ()=>{
    const result = server.src().pipe(server());
    return result.js.pipe(gulp.dest('server'));
});
gulp.task('build:server', (done)=>{
    runSequence(
        'clean:server',
        'copy:common',
        'compile:server', 
        done);
});

gulp.task('copy:common', ()=>gulp
    .src('./common/**/*', {base: '.'})
    .pipe(replace('// [common]', '\n//\n// THIS IS NOT THE FILE TO EDIT. FIND THE ONE IN THE /common DIRECTORY\n//\n'))
    .pipe(gulp.dest('./client/src'))
    .pipe(gulp.dest('./server'))
);

gulp.task('default', ['build:client', 'build:server']);
gulp.task('clean', ['clean:server', 'clean:client']);
